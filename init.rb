Redmine::Plugin.register :mt_reporting do
  name 'MT Reporting plugin'
  author 'Michael Josefski'
  description 'This is a plugin for Redmine for custom reporting for Mobile Technologies'
  version '0.0.1'
  url 'http://redmine.mobilet.com.au/redmine/mt_plugin'
  author_url 'http://www.mobilet.com.au'
end
