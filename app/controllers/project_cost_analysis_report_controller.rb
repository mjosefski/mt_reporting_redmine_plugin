class IssueCalculatedValues
  attr_accessor :time_spent
  attr_accessor :cost_incurred
  attr_accessor :chargeable_amount
  attr_accessor :non_chargeable_amount

  def initialize
    @time_spent = 0
    @cost_incurred = 0
    @chargeable_amount = 0
    @non_chargeable_amount = 0
  end
end

class IssueHierarchyItem
  attr_accessor :id
  attr_accessor :subject
  attr_accessor :indent
  attr_accessor :quoted_time
  attr_accessor :calculated_values

  def initialize

  end

  def quoted_amount
    quoted_time.to_f * 106
  end

  def remaining_budget
    quoted_amount - calculated_values.chargeable_amount
  end

end

class UserRate
  attr_accessor :user_id
  attr_accessor :cost_rate
  attr_accessor :charge_out_rate
  attr_accessor :rate_end_date

  def initialize(user_id, cost_rate, charge_out_rate, rate_end_date)
    @user_id = user_id
    @cost_rate = cost_rate
    @charge_out_rate = charge_out_rate
    @rate_end_date = rate_end_date
  end
end

class IssueHierarchy
  attr_accessor :project_identifier
  attr_accessor :issue_items
  attr_accessor :selected_project
  attr_accessor :total_time_spent
  attr_accessor :total_cost_incurred
  attr_accessor :total_chargeable_amount
  attr_accessor :total_non_chargeable_amount
  attr_accessor :user_rates
  attr_accessor :start_date
  attr_accessor :end_date
  
  def initialize()
    # Seed UserRates
    @user_rates = []
    # Trevor
    @user_rates << UserRate.new(66, 70, 106, nil)
    # Phillip
    @user_rates << UserRate.new(60, 66.66, 106, nil)
    # Nathan
    @user_rates << UserRate.new(69, 30.55, 53, nil)
    # Michael
    @user_rates << UserRate.new(6, 66.66, 106, nil)
    # Jacek
    @user_rates << UserRate.new(68, 33.33, 53, Date.parse('31-JUL-2015'))
    # Jacek latest rate
    @user_rates << UserRate.new(68, 41.66, 106, nil)
    # Hayden
    @user_rates << UserRate.new(50, 48.88, 90, nil)
    # Allen
    @user_rates << UserRate.new(9, 82.5, 106, nil)
    # Jonathan
    @user_rates << UserRate.new(72, 33.33, 53, nil)
    # Tom
    @user_rates << UserRate.new(74, 66.66, 106, nil)
    
  end

  def build(project_identifier, start_date, end_date)

    @project_identifier = project_identifier
    @start_date = start_date
    @end_date = end_date
    # Get the selected project
    @selected_project = Project.where(:identifier => @project_identifier).take(1)[0]

    # Get all issues for the project
    issues = Issue.where(:project_id => @selected_project.id).order('id asc')

    # Transpose the issues into a ViewModel that can be displayed on the view
    @issue_items = []
    @calculated_values = IssueCalculatedValues.new

    # For each issue calculate the time spent on the issue and all children issues during the specified time frame
    generate_issue_hierarchy(issues, @issue_items, nil, @calculated_values, @start_date, @end_date, 0, 3)

    @total_time_spent = @calculated_values.time_spent
    @total_cost_incurred = @calculated_values.cost_incurred
    @total_chargeable_amount = @calculated_values.chargeable_amount
    @total_non_chargeable_amount = @calculated_values.non_chargeable_amount


  end

  def generate_issue_hierarchy(issues, issue_hierarchy, parent_id, calculated_values, start_date, end_date, indent, levels_deep)

    issues.where(:parent_id => parent_id).each do |issue|

      issue_hierarchy_item = IssueHierarchyItem.new
      issue_hierarchy_item.id = issue.id
      issue_hierarchy_item.subject = issue.subject
      issue_hierarchy_item.indent = indent
      issue_hierarchy_item.quoted_time = issue.custom_field_value(9)
      issue_hierarchy_item.calculated_values = IssueCalculatedValues.new
      calculate_time_spent_and_costs(issue.id, start_date, end_date, issue_hierarchy_item.calculated_values)


      if (indent <= levels_deep - 1)
        issue_hierarchy << issue_hierarchy_item
      end

      generate_issue_hierarchy(issues, issue_hierarchy, issue.id, issue_hierarchy_item.calculated_values, start_date, end_date, indent + 1, levels_deep)

      calculated_values.time_spent += issue_hierarchy_item.calculated_values.time_spent
      calculated_values.cost_incurred += issue_hierarchy_item.calculated_values.cost_incurred
      calculated_values.chargeable_amount += issue_hierarchy_item.calculated_values.chargeable_amount
      calculated_values.non_chargeable_amount += issue_hierarchy_item.calculated_values.non_chargeable_amount

      
    end
  end

  def calculate_time_spent_and_costs(issue_id, start_date, end_date, calculated_values)

    # Get all TimeEntry records for the issue and calculate the total time spent on the issue
    if(start_date.nil? || end_date.nil?)
      time_entries = TimeEntry.where("issue_id = (?)", issue_id)
    else
      time_entries = TimeEntry.where("issue_id = (?) and spent_on >= (?) and spent_on <= (?)", issue_id, start_date.to_date, end_date.to_date)
    end
    
    time_entries.each { |time_entry| calculated_values.time_spent += time_entry.hours }

    time_entries.each { |time_entry| calculated_values.cost_incurred += time_entry.hours * get_user_rate(time_entry.user_id, time_entry.spent_on).cost_rate }

    # Custom Field 26 refers to the Non-chargeable field on the time entry screen
    time_entries.each do |time_entry|
      if time_entry.custom_field_value(26) != "1"
        calculated_values.chargeable_amount += time_entry.hours * get_user_rate(time_entry.user_id, time_entry.spent_on).charge_out_rate
      end
    end

    # Custom Field 26 refers to the Non-chargeable field on the time entry screen
    time_entries.each do |time_entry|
      if time_entry.custom_field_value(26) == "1"
        calculated_values.non_chargeable_amount += time_entry.hours * get_user_rate(time_entry.user_id, time_entry.spent_on).charge_out_rate
      end
    end

  end

  def get_user_rate(user_id, spent_on)
    # Check first for user_id
    # TODO: Implement the ability to handle rates based on date ranges.
    # Find a rate for the user where the date of the work performed is before the end_date of a rate range
    
    #Rails.logger.error "DEBUG: get_cost_rate: user_id = #{user_id}, spent_on = #{spent_on}"
    user = @user_rates.find {|user_rate| user_rate.user_id == user_id && !user_rate.rate_end_date.nil? && spent_on <= user_rate.rate_end_date}
    
    # If a rate can not be found within the date range then see if one exists with a nil end_dae_range
    if user.nil?
      #Rails.logger.error "DEBUG: Rate not found for user_id = #{user_id}"
      user = @user_rates.find {|user_rate| user_rate.user_id == user_id && user_rate.rate_end_date.nil?}
    end
    # If the user does not exist then use a default rate of 53
    # This is to cater for some users that we deleted. Namely Nathan and Johnathon. The rates used for thos users can be the lower rates. Ideally we should bring the users back and then go and update the data so that it is using the correct rates.
    if user.nil?
      #Rails.logger.error "DEBUG: Rate not found for user_id = #{user_id}. Using default rate."
      UserRate.new(0, 33.33, 53, nil)
    else 
       #Rails.logger.error "DEBUG: Rate found for user_id = #{user_id}. Using cost rate: #{user.cost_rate}, charge_out_rate: #{user.charge_out_rate}"
       user
    end  
  end
    
end


class ProjectCostAnalysisReportController < ApplicationController
  unloadable

  def show
    # Get all issues for the specificed project
    # Determine the total time spent per issue by summing all of the time spent on each issue. Eventually filter the times spent between two dates to determine the time spent during that period on the task
    # Display the issues in a heirarchy with the parent issue at the top in ascending order, with the children issues underneath

    # Get the project for bceonline

    # Transpose the issues into a ViewModel that can be exported to .csv
    @issue_hierarchy = IssueHierarchy.new()
    @issue_hierarchy.build(params[:project_identifier], params[:start_date], params[:end_date])

  end




end
